import { Routes } from '@angular/router';
import {OasisComponent } from '../../app/Oasis/oasis.component';
import { ErrorComponent } from '../error/error.component';
import { KanavaComponent } from '../Kanava/kanava.component';


export const appRoutes: Routes = [
    { path: 'oasis', component: OasisComponent },
    { path: 'kanava', component: KanavaComponent },
    { path: '',
      redirectTo: '/user',
      pathMatch: 'full'
    },
    { path: '**', component: ErrorComponent }
];
