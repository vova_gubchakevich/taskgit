import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HeroesComponent } from './heroes/heroes.component';
import { UserComponent } from './user/user.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { RouterModule, Routes } from '@angular/router';
import {OasisComponent} from '../app/Oasis/oasis.component';
import { appRoutes } from './routes/routes';
import { ErrorComponent } from './error/error.component';
import {KanavaComponent} from './Kanava/kanava.component';



@NgModule({
  declarations: [
    AppComponent,
    HeroesComponent,
    UserComponent,
    HeroDetailComponent,
    ErrorComponent,
    OasisComponent,
    KanavaComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

